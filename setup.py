from setuptools import setup, find_packages
import pathlib
import pkg_resources

with pathlib.Path('requirements.txt').open() as requirements_txt:
    install_requires = [
        str(requirement)
        for requirement
        in pkg_resources.parse_requirements(requirements_txt)
    ]

setup(
    name='mtgerate',
    version='0.1',
    install_requires=install_requires,
    packages=find_packages(),
    url='https://kunal-shah@bitbucket.org/mtge/mtgerate.git',
    license='README.md',
    author='kshah',
    author_email='kshah@pretium.com',
    description='mtge rate curve and discount curve package'
)
