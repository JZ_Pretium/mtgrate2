from mtgerate import curve_ust
import datetime as dt
import matplotlib.pyplot as plt


def plot_chart(ax, x_vals, y_vals, title="", label="", color='b'):
    ax.plot_date(x_vals, y_vals, label=f'{label}', c=color)
    # ax.set_xlim([0.0, 1.0])
    # ax.set_ylim([0.0, 1.0])
    ax.set_xlabel('dates')
    ax.set_title(f'{title}')
    ax.legend(loc="lower right")


if __name__ == '__main__':
    pricing_date = dt.date(2021, 1, 1)
    curve_handle = curve_ust.RateCurveUST(pricing_date)
    discount_curve = curve_handle.discount_curve()
    forward_curve_T2Y = curve_handle.forward_curve('2Y')
    forward_curve_T5Y = curve_handle.forward_curve('5Y')
    forward_curve_T10Y = curve_handle.forward_curve('10Y')

    fig, axs = plt.subplots(2, 1, figsize=(2 * 4, 1 * 8), tight_layout=True)
    plot_chart(axs[0], discount_curve.keys(), discount_curve.values(), "discount curve", color='k')
    plot_chart(axs[1], forward_curve_T2Y.keys(), [x * 100 for x in forward_curve_T2Y.values()], "Fwd", "T2Y")
    plot_chart(axs[1], forward_curve_T5Y.keys(), [x * 100 for x in forward_curve_T5Y.values()], "Fwd", "T5Y", color="r")
    plot_chart(axs[1], forward_curve_T10Y.keys(), [x * 100 for x in forward_curve_T10Y.values()], "Fwd", "T10Y", color="m")
    plt.show()