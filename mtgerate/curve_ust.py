import QuantLib as ql
import datetime as dt
import mtgerate.market_data as md
from functools import lru_cache
from typing import Optional
from dateutil.relativedelta import relativedelta


class RateCurveUST:
    def __init__(self, pricing_date: dt.date):
        self.pricing_date = pricing_date

    @staticmethod
    def calibration_instruments():
        return {"UST": ["1m", "2m", "3m", "6m", "1y", "2y", "3y", "5y", "7y", "10y", "20y", "30y"]}

    @staticmethod
    def tenor_months(tenor_string):
        period = float(tenor_string[:-1])
        multiplier = 12 if tenor_string[-1] == "y" else 1
        return period * multiplier

    @lru_cache()
    def market_data(self):
        mkt_data = md.interest_rate_hist_cache(self.pricing_date, self.pricing_date + relativedelta(days=-7))
        mkt_data = mkt_data[mkt_data.date == self.pricing_date]
        mkt_data.reset_index(drop=True, inplace=True)
        return mkt_data

    @classmethod
    def ql_conventions(cls):
        return {
            "calendar"       : ql.UnitedStates(ql.UnitedStates.GovernmentBond),
            "day_convention" : ql.ModifiedFollowing,
            "day_count"      : ql.Thirty360(),
            "EOM"            : True,
            "frequency"      : ql.Semiannual,
            "settlement_days": 2
        }

    def curve_schedule(self, eom=True):
        start_date = ql.Date.from_date(self.pricing_date)
        stub_start = ql.Date.from_date(self.pricing_date + relativedelta(day=31)) if eom else start_date
        end_date = ql.Date.from_date(self.pricing_date + relativedelta(years=30))

        schedule = ql.MakeSchedule(start_date, end_date, ql.Period('1m'),
                                   rule=ql.DateGeneration.Forward, endOfMonth=eom, firstDate=stub_start)

        return schedule

    def yield_curve_obj(self):
        instruments = self.calibration_instruments()["UST"]
        bond_maturities = []
        bond_rates = []
        conventions = self.ql_conventions()

        for instr in instruments:
            bond_maturities.append(ql.Period(instr))
            bond_rates.append(self.market_data().at[0, f't{str.lower(instr)}'])

        calib_date = ql.Date.from_date(self.pricing_date)
        ql.Settings.instance().evaluationDate = calib_date

        bond_helpers = []
        face_amount = 100
        for rate, maturity in zip(bond_rates, bond_maturities):
            termination_date = calib_date + maturity
            schedule = ql.Schedule(calib_date,
                                   termination_date,
                                   ql.Period(conventions["frequency"]),
                                   conventions["calendar"],
                                   conventions["day_convention"],
                                   conventions["day_convention"],
                                   ql.DateGeneration.Backward,
                                   conventions["EOM"])

            helper = ql.FixedRateBondHelper(ql.QuoteHandle(ql.SimpleQuote(face_amount)),
                                            conventions["settlement_days"],
                                            face_amount,
                                            schedule,
                                            [rate / 100],
                                            conventions["day_count"],
                                            conventions["day_convention"],
                                            )
            bond_helpers.append(helper)

        yieldcurve = ql.PiecewiseCubicZero(calib_date, bond_helpers, conventions["day_count"])

        return yieldcurve

    def discount_factor(self, dt_t: dt.date):
        conventions = self.ql_conventions()
        day_count = conventions["day_count"]
        calib_date = ql.Date.from_date(self.pricing_date)
        zero_date = ql.Date.from_date(dt_t)
        compounding = ql.Compounded
        frequency = conventions["frequency"]
        zero_rate = self.yield_curve_obj().zeroRate(zero_date, day_count, compounding, frequency)
        equivalent_rate = zero_rate.equivalentRate(day_count, compounding, frequency, calib_date, zero_date).rate()
        discount_factor = self.yield_curve_obj().discount(zero_date)
        return equivalent_rate, discount_factor

    def forward_rate(self, dt_t1: dt.date, dt_t2: Optional[dt.date] = None, tenor: Optional[str] = None):
        conventions = self.ql_conventions()
        day_count = conventions["day_count"]
        compounding = ql.Compounded
        frequency = conventions["frequency"]
        d1 = ql.Date.from_date(dt_t1)
        d2 = ql.Date.from_date(dt_t2) if dt_t2 else ql.Date.from_date(dt_t1) + ql.Period(tenor)
        yield_curve = self.yield_curve_obj()
        fwd_rate = yield_curve.forwardRate(d1, d2, day_count, compounding, frequency, True).rate()
        return fwd_rate

    def spot_curve(self, eom=True):
        spot_curve = {sch_dt.to_date(): self.discount_factor(sch_dt.to_date())[0]
                      for sch_dt in self.curve_schedule(eom=eom)}
        return spot_curve

    def discount_curve(self, eom=True):
        discount_curve = {sch_dt.to_date(): self.discount_factor(sch_dt.to_date())[1]
                          for sch_dt in self.curve_schedule(eom=eom)}
        return discount_curve

    def forward_curve(self, tenor: str, eom=True):
        forward_curve = {sch_dt.to_date(): self.forward_rate(sch_dt.to_date(), tenor=tenor)
                         for sch_dt in self.curve_schedule(eom=eom)}
        return forward_curve

