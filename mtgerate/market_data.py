from dateutil.relativedelta import relativedelta
import functools
import mtgeaws
import datetime as dt


@functools.lru_cache(maxsize=3, typed=False)
def interest_rate_cache(date_dt: dt):
    dt_str = date_dt.strftime("%Y%m%d")
    rate_query = f"select * " \
                 f"from mdl.interest_rates " \
                 f"where int_fcst_date = {dt_str}"
    data = mtgeaws.rds.query_pd(rate_query)
    data.columns = data.columns.str.lower()
    data = data.rename(columns={"date_dt": "date"})
    data["date"] = data["date"].values + relativedelta(days=1)
    return data


@functools.lru_cache(maxsize=3, typed=False)
def interest_rate_hist_cache(date_dt: dt, date_prior_dt: dt = dt.date.min):
    dt_str = date_dt.strftime("%Y%m%d")
    dt_prior_str = date_prior_dt.strftime("%Y%m%d")
    rate_query = f"select * " \
                 f"from mdl.interest_rates_hist " \
                 f"where date <= {dt_str} and date >= {dt_prior_str}"
    data = mtgeaws.rds.query_pd(rate_query)
    data.columns = data.columns.str.lower()
    return data